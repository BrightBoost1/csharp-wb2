﻿namespace ExamplesOOPWorkbook
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(MathStuff.PI);
            MathStuff m = new MathStuff(2);
            Console.WriteLine(MathStuff.PI);

            Console.WriteLine("Hello, World!");
            // object initialiser
            Person p = new Person("Name")
            {
                Age = 4
            };

            p.Age = -10;
            Console.WriteLine(p);

            Person[,] people = { 
                { new Person("123", "mark", 47), new Person("Dan") }, 
                { new Person("chuck"), new Person("rob") } 
            };

            Console.WriteLine(people[0, 0].ToString());
            Console.WriteLine(people[1, 0].Name);

        }
    }
}