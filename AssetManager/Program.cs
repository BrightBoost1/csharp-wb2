﻿namespace AssetManager
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Asset[] assets = new Asset[6];
            assets[0] = new Stock("1", "Microsoft", DateTime.Now, 150.0M, "MSFT", 147.0M, 10);
            assets[1] = new Stock("2", "Amazon", DateTime.Now, 130.0M, "AMZN", 99.0M, 20);
            assets[2] = new Stock("3", "Google", DateTime.Now, 120.0M, "GOOG", 97.0M, 30);
            assets[3] = new Car("4", "Ford Fusion", DateTime.Now, 7000.00M, 2013, 130000);
            assets[4] = new Car("5", "Honda Civic", DateTime.Now, 9000.00M, 2017, 150000);
            assets[5] = new Car("6", "Ford Focus", DateTime.Now, 10000.00M, 2021, 170000);

            foreach (Asset asset in assets) 
            {
                Console.WriteLine("Value: " + asset.GetValue());
                asset.WrapGetValue();
                if(asset is Car)
                {
                    Car c = (Car)asset;
                    Console.WriteLine(c.Description + " " + c.ModelYear + " " + c.OdometerReader);
                }
                if (asset is Stock)
                {
                    Stock s = (Stock)asset;
                    Console.WriteLine(s.Description + " " + s.CurrentSharePrice + " " + s.NumberOfShares);
                }
            }
        }

    }
}