﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Asset
    {
        public string AssetId { get; set; }
        public string Description { get; set; }
        public DateTime DateAcquired { get; set; }
        public decimal OriginalCost { get; set; }

        public Asset(string assetId, string description, DateTime dateAcquired, decimal originalCost)
        {
            AssetId = assetId;
            Description = description;
            DateAcquired = dateAcquired;
            OriginalCost = originalCost;
        }   

        public Asset() { }

        public virtual decimal GetValue()
        {
            return OriginalCost;
        }

        public void WrapGetValue()
        {
            Console.WriteLine(OriginalCost + " " + this.GetValue());
        }
    }
}
