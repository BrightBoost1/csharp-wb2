﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExamples
{
    public class OfficeWorker : Worker
    {
        public string NameOffice { get; set; }

        public OfficeWorker(string name, int age, string jobTitle, decimal pay, string nameOffice) : base(name, age, jobTitle, pay)
        {
            NameOffice = nameOffice;
        }

        public override void DescribeObject()
        {
            base.DescribeObject();
            Console.WriteLine("Office worker: " + NameOffice);
        }
    }
}
