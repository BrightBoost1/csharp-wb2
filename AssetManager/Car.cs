﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Car : Asset
    {
        public int ModelYear { get; set; }
        public int OdometerReader { get; set; }

        public Car(string assetId, string description, DateTime dateAcquired, decimal originalCost, int modelYear, int odometerReading) : base(assetId, description, dateAcquired, originalCost)
        {
            ModelYear = modelYear;
            OdometerReader = odometerReading;
        }

        public override decimal GetValue()
        {
            DateTime today = DateTime.Now;
            int yearsOld = today.Year - ModelYear;
            if(yearsOld < 7)
            {
                double percentage = 1 - ((OdometerReader / 5000.0) * 0.02);
                if(percentage < 0.9)
                {
                    return (decimal) percentage * OriginalCost;
                }
                else
                {
                    return 0.9M * OriginalCost;
                }
            } else
            {
                return OdometerReader < 100000 ? 0.3M * OriginalCost : 0.1M * OriginalCost;
            }
        }
    }
}
