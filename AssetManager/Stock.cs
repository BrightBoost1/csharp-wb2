﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Stock : Asset
    {
        public string StockTicker { get; set; }
        public decimal CurrentSharePrice { get; set; }
        public int NumberOfShares { get; set; }

        public Stock(string assetId, string description, DateTime dateAcquired, decimal originalCost, string stockTicker, decimal currentSharePrice, int numberOfShares) : base(assetId, description, dateAcquired, originalCost)
        {
            StockTicker = stockTicker;
            CurrentSharePrice = currentSharePrice;
            NumberOfShares = numberOfShares;
        }

        public override decimal GetValue()
        {
            return NumberOfShares * CurrentSharePrice;
        }
    }
}
