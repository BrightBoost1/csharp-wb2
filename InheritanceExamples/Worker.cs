﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExamples
{
    public class Worker : Person
    {
        public string JobTitle { get; set; }
        public decimal Pay { get; set; }

        public Worker(string name, int age, string jobTitle, decimal pay) : base(name, age)
        {
            JobTitle = jobTitle;
            Pay = pay;
        }

        public override void DescribeObject()
        {
            base.DescribeObject();
            Console.WriteLine("This is a worker: " + JobTitle);
        }

    }
}
