﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExamples
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public virtual void DescribeObject()
        {
            Console.WriteLine("This is a person: " + Name);
        }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}
