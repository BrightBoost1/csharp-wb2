﻿using System.Net.Http.Headers;

namespace InheritanceExamples
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person("Zeph", 11);
            Worker worker = new Worker("Zach", 18, "route manager", 40000);
            OfficeWorker officeWorker = new OfficeWorker("Bob", 31, "Office person", 400000, "initech");

            Person p = new Worker("Maarik", 32, "does stuff", 123432);
            p = new Person("Ali", 67);
            Console.WriteLine(((Worker)p).JobTitle);

            person.DescribeObject();
            worker.DescribeObject();
            officeWorker.DescribeObject();

            DisplayPerson(person);
            DisplayPerson(worker);
        }

        static void DisplayPerson(Person person)
        {
            Console.WriteLine(person.Name);
            Console.WriteLine(person.Age);
            if(person is Worker)
            {
                Console.WriteLine(((Worker)person).JobTitle);
            }
        }

    }
}