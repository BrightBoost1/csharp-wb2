﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamplesOOPWorkbook
{
    public class Person
    {
        private string id;
        // Properties
        public string Name { get; set; }
        public int Age { get; set; }
        public const string planet = "Earth";

        public Person(string id, string name, int age) 
        {
            this.id = id;
            Name = name;
            Age = age;
        }

        public Person(string name) 
        { 
            this.Name = name;
        }

        public override string ToString()
        {
            return "Age: " + this.Age + " Planet: " + planet;
        }

    }
    // instantiating an objecting
    
}
