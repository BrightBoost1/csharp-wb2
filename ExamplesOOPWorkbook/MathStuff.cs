﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamplesOOPWorkbook
{
    public class MathStuff
    {
        public const double PI = Math.PI;
        public int X { get; set; }
        public MathStuff(int x)
        {
            X = x;
        }
        public static double Add(double a, double b)
        {
            return a + b;
        }

        public static double CircumferenceCircle(double r)
        {
            return 2 * PI * r;
        }

        public static double AreaCircle(double r)
        {
            return PI * (r * r);
        }

    }
}
